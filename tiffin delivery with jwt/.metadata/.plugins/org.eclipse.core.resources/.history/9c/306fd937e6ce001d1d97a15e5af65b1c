package com.tiffin.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiffin.dtos.ApiResponse;
import com.tiffin.dtos.AreasDto;
import com.tiffin.services.AreaService;

@RestController
@RequestMapping("/tiffin/area")
public class AreaController {
	@Autowired
	private AreaService areaService;
	
	@PostMapping("/add")
	public ResponseEntity<AreasDto> signUp(@RequestBody AreasDto areaDto)
	{
		AreasDto addArea = this.areaService.createArea(areaDto);
		return new ResponseEntity<>(addArea,HttpStatus.CREATED);
		
	}
	
//PUT
	@PutMapping("/edit/{areaId}")
	public ResponseEntity<AreasDto> updateArea(@RequestBody AreasDto areaDto, @PathVariable Integer areaId){
		AreasDto updatedArea = this.areaService.updateArea(areaDto, areaId);
		return ResponseEntity.ok(updatedArea);
	}

//DELETE
	@DeleteMapping("/delete/{areaId}")
	public ResponseEntity<ApiResponse> deleteArea(@PathVariable Integer areaId){
		this.areaService.deleteArea(areaId);
		return new ResponseEntity<ApiResponse>(new ApiResponse("Area deleted successfully",true),HttpStatus.OK);
	}
//GET
	@GetMapping("/display/all")
	public ResponseEntity<List<AreasDto>> displayAllAreas() {
		return ResponseEntity.ok(this.areaService.getAllAreas());
	}
	
	@GetMapping("/display/{areaId}")
	public ResponseEntity<AreasDto> getTiffinById(@PathVariable Integer areaId){
		return ResponseEntity.ok(this.areaService.getAreaById(areaId));
	}
}
