package com.tiffin.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiffin.dtos.TiffinsDto;
import com.tiffin.entities.Tiffins;
import com.tiffin.exceptions.ResourceNotFoundException;
import com.tiffin.repos.TiffinRepo;
import com.tiffin.services.TiffinService;

@Service
public class TiffinServiceImpl implements TiffinService{
	@Autowired
	private TiffinRepo tiffinRepo;
	@Autowired
	private ModelMapper modelMapper;
	@Override
	public TiffinsDto createTiffin(TiffinsDto tiffinDto) {
		// TODO Auto-generated method stub
		Tiffins tiffin = this.dtoToTiffin(tiffinDto);
		Tiffins savedTiffin = this.tiffinRepo.save(tiffin);
		return this.tiffinToDto(savedTiffin);
	}

	@Override
	public TiffinsDto updateTiffin(TiffinsDto tiffinDto, Integer tiffinId) {
		// TODO Auto-generated method stub
		Tiffins updatedTiffin = this.tiffinRepo.findById(tiffinId).orElseThrow(()-> new ResourceNotFoundException("Tiffin","tiffinId",tiffinId));
		updatedTiffin.setType(tiffinDto.getType());
		updatedTiffin.setDescription(tiffinDto.getDescription());
		updatedTiffin.setImage(tiffinDto.getImage());
		updatedTiffin.setPrice(tiffinDto.getPrice());
		
		Tiffins updateSavedTiffins = this.tiffinRepo.save(updatedTiffin);
		TiffinsDto updatedTiffinsDto = this.tiffinToDto(updateSavedTiffins);
		
		return updatedTiffinsDto;
	}

	@Override
	public TiffinsDto getTiffinById(Integer tiffinId) {
		// TODO Auto-generated method stub
		Tiffins tiffin = this.tiffinRepo.findById(tiffinId).orElseThrow(()-> new ResourceNotFoundException("Tiffins","tiffinId",tiffinId));
		return this.tiffinToDto(tiffin);
	}

	@Override
	public List<TiffinsDto> getAllTiffins() {
		// TODO Auto-generated method stub
		List<Tiffins> tiffins = this.tiffinRepo.findAll();
		List<TiffinsDto> tiffinsDto = tiffins.stream().map(tiffin->this.tiffinToDto(tiffin)).collect(Collectors.toList());
		return tiffinsDto;
	}

	@Override
	public void deleteTiffin(Integer tiffinId) {
		// TODO Auto-generated method stub
		Tiffins tiffin = this.tiffinRepo.findById(tiffinId).orElseThrow(()-> new ResourceNotFoundException("Tiffins","tiffinId",tiffinId));
		this.tiffinRepo.delete(tiffin);
	}
	public Tiffins dtoToTiffin(TiffinsDto tiffinDto)
	{
		Tiffins tiffin = this.modelMapper.map(tiffinDto, Tiffins.class);
		
		return tiffin;
	}
	public TiffinsDto tiffinToDto(Tiffins tiffin)
	{
		TiffinsDto tiffinDto = this.modelMapper.map(tiffin, TiffinsDto.class);
		
		return tiffinDto;
	}

}
