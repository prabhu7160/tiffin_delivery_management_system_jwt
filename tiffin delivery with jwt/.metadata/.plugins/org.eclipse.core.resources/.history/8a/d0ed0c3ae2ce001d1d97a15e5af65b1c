package com.tiffin.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiffin.dtos.AreasDto;
import com.tiffin.entities.Areas;
import com.tiffin.exceptions.ResourceNotFoundException;
import com.tiffin.repos.AreaRepo;
import com.tiffin.services.AreaService;

@Service
public class AreaServiceImpl implements AreaService{
	@Autowired
	private AreaRepo areaRepo;
	@Autowired
	private ModelMapper modelMapper;
	@Override
	public AreasDto createArea(AreasDto areaDto) {
		// TODO Auto-generated method stub
		Areas area = this.dtoToArea(areaDto);
		Areas savedArea = this.areaRepo.save(area);
		return this.areaToDto(savedArea);
	}

	@Override
	public AreasDto updateArea(AreasDto areaDto, Integer areaId) {
		// TODO Auto-generated method stub
		Areas updatedArea = this.areaRepo.findById(areaId).orElseThrow(()-> new ResourceNotFoundException("Area","areaId",areaId));
		updatedArea.setState(areaDto.getState());
		updatedArea.setCity(areaDto.getCity());
		updatedArea.setPincode(areaDto.getPincode());
		updatedArea.setStreetAddress(areaDto.getStreetAddress());
		updatedArea.setHouseName(areaDto.getHouseName());
		updatedArea.setRoomNumber(areaDto.getRoomNumber());
		
		Areas updateSavedArea = this.areaRepo.save(updatedArea);
		AreasDto updatedAreaDto = this.areaToDto(updateSavedArea);
		
		return updatedAreaDto;
	}

	@Override
	public AreasDto getAreaById(Integer areaId) {
		// TODO Auto-generated method stub
		Areas updatedArea = this.areaRepo.findById(areaId).orElseThrow(()-> new ResourceNotFoundException("Area","areaId",areaId));
		return this.areaToDto(updatedArea);
	}

	@Override
	public List<AreasDto> getAllAreas() {
		// TODO Auto-generated method stub
		List<Areas> areas = this.areaRepo.findAll();
		List<AreasDto> areasDto = areas.stream().map(area->this.areaToDto(area)).collect(Collectors.toList());
		return areasDto;
	}

	@Override
	public void deleteArea(Integer areaId) {
		// TODO Auto-generated method stub
		Areas deletedArea = this.areaRepo.findById(areaId).orElseThrow(()-> new ResourceNotFoundException("Area","areaId",areaId));
		this.areaRepo.delete(deletedArea);
	}
	public Areas dtoToArea(AreasDto areaDto)
	{
		Areas area = this.modelMapper.map(areaDto, Areas.class);
		
		return area;
	}
	public AreasDto areaToDto(Areas area)
	{
		AreasDto areaDto = this.modelMapper.map(area, AreasDto.class);
		
		return areaDto;
	}
}
